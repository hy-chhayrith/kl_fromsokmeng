package com.example.sokmeng.listview1

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.ListView

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var ListView = findViewById<ListView>(R.id.ListView1)

        var arrList = ArrayList<String>()

        arrList.add("sokmeng")
        arrList.add("sokmeng1")
        arrList.add("sokmeng2")
        arrList.add("sokmeng3")
        //we use adapter for the property to contain the arrList and then we can cell it by use ListView.adapter
        var kaka = ArrayAdapter(this,android.R.layout.simple_expandable_list_item_1, arrList)

        ListView.adapter = kaka

    }
}
