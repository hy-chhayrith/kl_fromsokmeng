package com.example.sokmeng.androidrecyclerviewandcardview;

public class Product {
    private int id;
    private String title, shortdesc;
    private double rating;
    private double price;
    private int image;

    public Product(int id,String title,String shortdesc,double rating,double price,int image)
    {
        this.id = id;
        this.title = title;
        this.shortdesc = shortdesc;
        this.rating = rating;
        this.price = price;
        this.image = image;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", shortdesc='" + shortdesc + '\'' +
                ", rating=" + rating +
                ", price=" + price +
                ", image=" + image +
                '}';
    }
}
