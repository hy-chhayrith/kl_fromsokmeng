package com.example.sokmeng.customview1

import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ListView
import android.widget.TextView
import com.example.sokmeng.customview1.R.id.parent
import java.security.AccessControlContext
import java.util.ArrayList

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        var listView = findViewById<ListView>(R.id.ListVew1)

        var adapter = mycustomadapter(this)
        listView.adapter = adapter
    }



    class mycustomadapter(context: Context):BaseAdapter()
    {
        //var name = arrayListOf<String>("kaka","sala","tolo") this is the fist code

        var names = arrayListOf<String>( "Sothea","Ratanawan","Lynan","Manich","Dalya")
        var emails = arrayListOf<String>( "Sothea@gmail.com","Ratanawan@gmail.com","Lynan@gmail.com","Manich@gmail.com","Dalya@gmail.com")
        var mobiles = arrayListOf<String>( "98774994","9940944","65356363","76464","994849")


        var mContaxt:Context // mycontaxt is the name and Context is the datatype
        init {
            mContaxt = context
        }
        override fun getCount(): Int {
            return names.count()
        }

        override fun getItemId(p0: Int): Long {
            return p0.toLong()
        }

        override fun getItem(p0: Int): Any {
           return p0
        }

        override fun getView(p0: Int, p1: View?, p2: ViewGroup?): View {
//            var tv=TextView(mContaxt)
//            tv.text = names.get(p0)
//            return tv this is fist code
            var inflater = LayoutInflater.from(mContaxt)
                var view = inflater.inflate(R.layout.lay_1, p2,false)
                var name = view.findViewById<TextView>(R.id.name)
                name.text = names.get(p0)
                var email = view.findViewById<TextView>(R.id.email)
                email.text = emails.get(p0)
                var mobile = view.findViewById<TextView>(R.id.mobile)
                mobile.text = mobiles.get(p0)
                return view
        }
    }
}
