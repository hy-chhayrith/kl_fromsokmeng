package com.example.sokmeng.customview1

import android.provider.ContactsContract

data class Stud(var name:String, var mobile:String , var email:String = name+"@gmail.com")

object StudentContainer
{
    var arrList = listOf<Stud>(

            Stud("Ratanak","87774884"),
            Stud("Ratanak1","877748842"),
            Stud("Ratanak2","877748843"),
            Stud("Ratanak3","877748844")
    )
}